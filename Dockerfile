FROM python:3.7-alpine

RUN apk update && apk upgrade -U

RUN pip install -U pip

# less
RUN apk add --no-cache --update npm
RUN npm install -no-cache -g less

RUN apk add --no-cache --virtual .build-deps build-base linux-headers

# gdal
ARG GDAL_VERSION=2.4.1
RUN wget http://download.osgeo.org/gdal/${GDAL_VERSION}/gdal-${GDAL_VERSION}.tar.gz
RUN tar xzf gdal-${GDAL_VERSION}.tar.gz \
 && cd gdal-${GDAL_VERSION} \
 && ./configure --with-python \
 && make \
 && make install \
 && cd .. \
 && gdal-${GDAL_VERSION}*

# geos
ARG GEOS_VERSION=3.7.1
RUN wget http://download.osgeo.org/geos/geos-${GEOS_VERSION}.tar.bz2
RUN tar xjf geos-${GEOS_VERSION}.tar.bz2 \
 && cd geos-${GEOS_VERSION} \
 && ./configure \
 && make \
 && make install \
 && cd .. \
 && geos-${GEOS_VERSION}*

# psycopg2
RUN apk add --no-cache postgresql-dev

# pillow
RUN apk add --no-cache jpeg-dev zlib-dev

RUN cd / && rm -rf /var/cache/apk/*
