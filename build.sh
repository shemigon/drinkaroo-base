#!/usr/bin/env bash

NAME=shemigon/$(basename $(pwd))

docker build -t $NAME .
docker push $NAME
